export interface UfdrData {
  project: Project;
  metadata: UfdrMetada[];
  decodedData: UfdrDecodedData[];
  taggedFiles: UfdrTaggedFile[];
}

export interface UfdrDecodedData {
  type: UfdrDecodedDataType;
  items: UfdrDecodedItem[];
}

export interface UfdrDecodedItem {
  id: string;
  type: UfdrDecodedDataType;
  data: UfdrDecodedItemData[];
}

export interface UfdrDecodedItemData {
  name: UfdrDecodedName;
  type: UfdrDecodedDataType;
  value: any;
}

export enum UfdrDecodedName {
  AccessGroup = "AccessGroup",
  Account = "Account",
  ActivationCount = "ActivationCount",
  ActiveTime = "ActiveTime",
  AggregatedLocationsCount = "AggregatedLocationsCount",
  AppGUID = "AppGUID",
  AppSize = "AppSize",
  Application = "Application",
  Availability = "Availability",
  BSSID = "BSSId",
  BackgroundTime = "BackgroundTime",
  Body = "Body",
  CanRebuildCacheFile = "CanRebuildCacheFile",
  Category = "Category",
  Class = "Class",
  Confidence = "Confidence",
  Copyright = "Copyright",
  CountryCode = "CountryCode",
  CreationTime = "CreationTime",
  Data = "Data",
  DataType = "DataType",
  DateCreated = "DateCreated",
  DateModified = "DateModified",
  DecodingStatus = "DecodingStatus",
  DeletedDate = "DeletedDate",
  Delivered = "Delivered",
  Description = "Description",
  Details = "Details",
  Direction = "Direction",
  Domain = "Domain",
  Duration = "Duration",
  EffectiveUID = "EffectiveUID",
  EndDate = "EndDate",
  EndTime = "EndTime",
  Expiry = "Expiry",
  Folder = "Folder",
  GenericAttribute = "GenericAttribute",
  Group = "Group",
  ID = "Id",
  IDInOrigin = "IdInOrigin",
  Identifier = "Identifier",
  Info = "Info",
  InstallDate = "InstallDate",
  IsEmulatable = "IsEmulatable",
  Label = "Label",
  LastAccessTime = "LastAccessTime",
  LastActivity = "LastActivity",
  LastAutoConnection = "LastAutoConnection",
  LastConnected = "LastConnected",
  LastConnection = "LastConnection",
  LastLaunched = "LastLaunched",
  LastModified = "LastModified",
  LastVisited = "LastVisited",
  LaunchCount = "LaunchCount",
  Location = "Location",
  MACAddress = "MACAddress",
  Map = "Map",
  NWConnectionType = "NWConnectionType",
  Name = "Name",
  NetworkCode = "NetworkCode",
  NetworkName = "NetworkName",
  OperationMode = "OperationMode",
  Origin = "Origin",
  PID = "PID",
  Package = "Package",
  Password = "Password",
  Path = "Path",
  PositionAddress = "PositionAddress",
  Precision = "Precision",
  Priority = "Priority",
  PurchaseDate = "PurchaseDate",
  Quantity = "Quantity",
  Read = "Read",
  RelatedApplication = "RelatedApplication",
  Reminder = "Reminder",
  RepeatDay = "RepeatDay",
  RepeatInterval = "RepeatInterval",
  RepeatRule = "RepeatRule",
  RepeatUntil = "RepeatUntil",
  SSID = "SSId",
  SampleSource = "SampleSource",
  SecurityMode = "SecurityMode",
  Server = "Server",
  ServerAddress = "ServerAddress",
  Service = "Service",
  ServiceType = "ServiceType",
  Severity = "Severity",
  Smsc = "SMSC",
  Source = "Source",
  StartDate = "StartDate",
  StartTime = "StartTime",
  Status = "Status",
  Subject = "Subject",
  Tid = "TID",
  TimeContacted = "TimeContacted",
  TimeCreated = "TimeCreated",
  TimeModified = "TimeModified",
  TimeStamp = "TimeStamp",
  TimesContacted = "TimesContacted",
  Title = "Title",
  Type = "Type",
  URL = "Url",
  URLCacheFile = "UrlCacheFile",
  Units = "Units",
  UserMapping = "UserMapping",
  Username = "Username",
  Value = "Value",
  Version = "Version",
  VideoCall = "VideoCall",
  VisitCount = "VisitCount",
}

export enum UfdrDecodedDataType {
  ApplicationOperationMode = "ApplicationOperationMode",
  Boolean = "Boolean",
  CallType = "CallType",
  ContactType = "ContactType",
  DecodingStatus = "DecodingStatus",
  Double = "Double",
  EventAvailability = "EventAvailability",
  EventClass = "EventClass",
  EventPriority = "EventPriority",
  EventStatus = "EventStatus",
  Int32 = "Int32",
  Int64 = "Int64",
  LocationOrigin = "LocationOrigin",
  LogSeverity = "LogSeverity",
  MailPriority = "MailPriority",
  MessageStatus = "MessageStatus",
  ModelDirections = "ModelDirections",
  NWConnectionType = "NWConnectionType",
  PasswordType = "PasswordType",
  RepeatDay = "RepeatDay",
  RepeatRule = "RepeatRule",
  SampleDataType = "SampleDataType",
  SampleSourceType = "SampleSourceType",
  String = "String",
  TelephonyCallStatus = "TelephonyCallStatus",
  TimeSpan = "TimeSpan",
  TimeStamp = "TimeStamp",
  UInt32 = "UInt32",
  Units = "Units",
}

export enum UfdrDecodedDataType {
  Activity = "Activity",
  BluetoothDevice = "BluetoothDevice",
  CalendarEntry = "CalendarEntry",
  Call = "Call",
  Chat = "Chat",
  Contact = "Contact",
  Cookie = "Cookie",
  InstalledApplication = "InstalledApplication",
  Location = "Location",
  LogEntry = "LogEntry",
  Mms = "MMS",
  Password = "Password",
  SMS = "SMS",
  UserAccount = "UserAccount",
  VisitedPage = "VisitedPage",
  WebBookmark = "WebBookmark",
  WirelessNetwork = "WirelessNetwork",
}

export interface UfdrMetada {
  section: string;
  item: UfdrMetaItem[];
}

export interface UfdrMetaItem {
  name: string;
  type: string;
  value: boolean | string;
}

export interface Project {
  id: string;
  name: string;
  reportVersion: string;
  licenseID: number;
  containsGarbage: string;
  extractionType: string;
  NodeCount: number;
  ModelCount: number;
  xmlns: string;
}

export interface UfdrTaggedFile {
  fs: string;
  fsId: string;
  size: number;
  accessDates: TaggedFileAccessDate[];
  metadata: TaggedFileMetadata[];
}

export interface TaggedFileAccessDate {
  name: AccessDateName;
  value: string;
}

export enum AccessDateName {
  CreationTime = "CreationTime",
  ModifyTime = "ModifyTime",
}

export interface TaggedFileMetadata {
  item: TaggedFileItem[];
}

export interface TaggedFileItem {
  name: string;
  type: string;
  value: string;
}
