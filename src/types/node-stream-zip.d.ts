declare module 'node-stream-zip' {
  import { Readable } from 'stream';

  interface StreamZipOptions {
    file: string;
    storeEntries: boolean;
    chunkSize?: number;
    skipEntryNameValidation?: boolean;
  }

  type EventCallback<T> = (data: T) => void;
  type DataCallback<T> = (error: any, data: T) => void;

  export default class StreamZip {
    entriesCount: string;
    constructor(opts: StreamZipOptions);
    entries(): any[];
    close() : any;
    stream(entry: string, cb: DataCallback<Readable>): void;
    entryDataSync(entry: string): any;
    extract(emtryPath: string | null, outputPath: string, cb: EventCallback<any>): void;

    on(event: 'error', cb: EventCallback<any>): void;
    on(event: 'entry', cb: EventCallback<any>): void;
    on(event: string, cb: Function): StreamZip;
  }
}
