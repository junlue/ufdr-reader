import StreamZip from "node-stream-zip";

export const open = (zipFile: string): Promise<StreamZip> => {
  return new Promise((resolve, reject) => {
    const zip = new StreamZip({ file: zipFile, storeEntries: true });
    zip.on("ready", () => {
      resolve(zip);
    });

    zip.on("error", (err) => reject(err));
  });
};

export const extract = async (
  zipFile: string | StreamZip,
  outputDir: string,
  entryPath?: string
): Promise<void> => {
  const zip = typeof zipFile === "string" ? await open(zipFile) : zipFile;

  return new Promise((resolve, reject) => {
    zip.extract(entryPath || null, outputDir, (err: any) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

export const entry = async (
  zipFile: string | StreamZip,
  entryPath: string
): Promise<any> => {
  const zip = typeof zipFile === "string" ? await open(zipFile) : zipFile;
  const data = zip.entryDataSync(entryPath);

  return data;
};
