import { parse, validate } from "fast-xml-parser";
import { decode } from "he";

export const xmlToJson = (xml: string): any => {
  if (!validate(xml)) {
    return;
  }

  const data = parse(xml, {
    attributeNamePrefix: "",
    attrNodeName: "attr",
    textNodeName: "text",
    ignoreAttributes: false,
    ignoreNameSpace: false,
    allowBooleanAttributes: true,
    parseNodeValue: true,
    parseAttributeValue: true,
    trimValues: true,
    cdataTagName: "__cdata",
    cdataPositionChar: "\\c",
    parseTrueNumberOnly: false,
    arrayMode: false,
    attrValueProcessor: (val: any) => decode(val, { isAttributeValue: true }),
    tagValueProcessor: (val: any) => decode(val),
    stopNodes: ["parse-me-as-string"],
  });

  return data;
};
