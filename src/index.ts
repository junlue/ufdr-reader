import * as unzip from "./unzip";
import { xmlToJson } from "./xml";
import { UfdrData } from "./types/ufdr";

const getNodeValue = (data: any, node: string | string[]): any => {
  const nodes = Array.isArray(node) ? node : node.split('.');

  let n, value = data;
  while(n = nodes.shift()) {
    value = value[n];
  }

  return value;
};

const arrayable = (data: any): any[] => {
  if (Array.isArray(data)) {
    return data;
  } else if (typeof data === "object") {
    return [data];
  }

  return [];
};

const arrayableNode = (data: any, node: string | string[]): any => {
  const value = getNodeValue(data, node);

  return arrayable(value);
};

const getParsedValue = (data: any): any => {
  if (data) {
    return data.__cdata || data.text;
  }
};

const guessValue = (data: any) => {
  const value = getParsedValue(data);

  if (typeof value === "string" && value.length) {
    const v = value.toLowerCase();
    if (v === "true") {
      return true;
    } else if (v === "false") {
      return false;
    }
  } else if (!isNaN(+value)) {
    return +value || 0;
  }

  return value || '';
};

const parseValue = (data: any, dataType: string): any => {
  const value = getParsedValue(data);
  const type = dataType ? dataType.toLowerCase() : '';
  const numericFields = ["double", "integer", "float", "uint32", "units"];
  const dateFields = ["date", "timestamp"];

  if (type === "boolean") {
    return value === "True" || value === "true" || !!+value;
  } else if (numericFields.includes(type)) {
    return +value || 0;
  } else if (dateFields.includes(type)) {
    return new Date(value);
  }

  return guessValue(data);
};

export const readUfdr = async (file: string): Promise<UfdrData> => {
  const zip = await unzip.open(file);
  const buf = await unzip.entry(zip, "report.xml");
  const parsed = xmlToJson(buf.toString());

  const data: any = {};
  const project = parsed && parsed.project ? parsed.project : null;
  if (project) {
    data.project = project.attr;
    data.case = getNodeValue(project, 'caseInformation.attr');
    data.metadata = arrayableNode(project, 'metadata').map((m: any) => ({
      section: m.attr.section,
      item: m.item.map((i: any) => ({
        name: i.attr.name,
        type: i.attr.systemtype,
        value: guessValue(i),
      })),
    }));
    data.decodedData = arrayableNode(project, 'decodedData.modelType').map((d: any) => ({
      type: d.attr.type,
      items: d.model.map((m: any) => ({
        id: m.attr.id,
        type: m.attr.type,
        data: m.field.map((f: any) => ({
          name: f.attr.name,
          type: f.attr.type,
          value: parseValue(f.value, f.attr.type),
        })),
      })),
    }));
    data.taggedFiles = arrayableNode(project, 'taggedFiles.file').map((f: any) => ({
      fs: f.attr.fs,
      fsId: f.attr.fsid,
      size: f.attr.size,
      accessDates: arrayableNode(f, 'accessInfo.timestamp').map((t: any) => ({
        name: t.attr.name,
        value: t.text,
      })),
      metadata: arrayableNode(f, 'metadata').map((m: any) => ({
        section: m.section,
        item: m.item.map((i: any) => ({
          name: i.attr.name,
          type: i.attr.systemtype,
          value: guessValue(i),
        })),
      })),
    }));
  }

  zip.close();

  return data;
};
