import { expect } from 'chai';
import { readUfdr } from '../src';
import path from 'path';

describe('ufdr', () => {
  it('can read ufdr file', async () => {
    const data = await readUfdr(path.resolve(__dirname, 'files/iphone.ufdr'));

    expect(data).to.not.be.undefined;
  });
});
